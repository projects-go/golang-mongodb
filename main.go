package main

import (
	"fmt"

	"github.com/kr/pretty"
	"gitlab.com/arnaldoceballos/golang-mongodb/data"
	"gitlab.com/arnaldoceballos/golang-mongodb/models/user"
)

func main() {
	data.InitMongo()
	user.SetStorage(data.MongoUserStorage{})
	c := data.NewContextMongo()
	c.DBCollectionMongo("users").RemoveAll(nil)
	defer c.CloseMongo()

	/* 	fmt.Println("*************** CREATE ***************")
	   	u := &user.User{
	   		UserName: "Arnaldo",
	   		Email:    "arnaldoceballos@gmail.com",
	   		Password: "123456",
	   	}
	   	err := u.Create()
	   	if err != nil {
	   		log.Println(err)
	   	}
	   	pretty.Printf("Usuario%# v:", u)

	   	fmt.Println("*************** GET BY ID ***************")
	   	u2 := user.New()
	   	u2.ID = u.ID
	   	err = u2.GetByID()
	   	if err != nil {
	   		log.Println(err)
	   	}
	   	pretty.Printf("Usuario%# v:", u2)

	   	fmt.Println("*************** UPDATE ***************")
	   	u.UserName = "Raúl"
	   	u.Email = "raul@gmail.com"
	   	err = u.Update()
	   	if err != nil {
	   		log.Println(err)
	   	}
	   	pretty.Printf("Usuario%# v:", u)

	   	fmt.Println("*************** DELETE ***************")
	   	err = u.Delete()
	   	if err != nil {
	   		log.Println(err)
	   	} else {
	   		fmt.Println("Eliminado correctamente.")
	   	} */

	nombres := []string{
		"Arnaldo",
		"Raúl",
		"Carlos",
		"Luis",
		"Leonardo",
		"Agustín",
		"Osvaldo",
		"Javier",
	}
	for _, nombre := range nombres {
		u := &user.User{
			UserName: nombre,
			Email:    nombre + "@gmail.com",
			Password: nombre + "123456",
		}
		err := u.Create()
		if err != nil {
			fmt.Println(err)
		}
	}
	u := user.New()
	users, err := u.GetAll()
	if err != nil {
		fmt.Println(err)
	}
	pretty.Printf("USUARIOS: %# v", users)

	for _, nombre := range nombres {
		u := &user.User{
			UserName: nombre,
			Email:    nombre + "@gmail.com",
			Password: nombre + "123456",
		}
		err := u.Create()
		if err != nil {
			fmt.Println(err)
		}
	}
	u = user.New()
	users, err = u.GetAll()
	if err != nil {
		fmt.Println(err)
	}
	pretty.Printf("TODOS LOS USUARIOS: %# v", users)
}
