package data

import (
	"gopkg.in/mgo.v2"
)

type ContextMongo struct {
	Session *mgo.Session
}

func (c *ContextMongo) CloseMongo() {
	c.Session.Close()
}

func (c *ContextMongo) DBCollectionMongo(name string) *mgo.Collection {
	return c.Session.DB(DBNAME).C(CNAME)
}

func NewContextMongo() *ContextMongo {
	session := getSessionMongo().Copy()
	c := &ContextMongo{
		Session: session,
	}
	return c
}
