package data

import (
	"log"

	"gopkg.in/mgo.v2"
)

const (
	DBNAME = "GOLANG"
)

var session *mgo.Session

func createDBSessionMongo() {
	var err error
	session, err = mgo.Dial("localhost")
	if err != nil {
		log.Fatal(err)
	}
}

func getSessionMongo() *mgo.Session {
	if session == nil {
		createDBSessionMongo()
	}
	return session
}

func addIndex() {
	userIndex := mgo.Index{
		Key:        []string{"username", "email"},
		Unique:     true, // No se pueden repetir los valores
		Background: true, // si el proceso no es rápido, para no detener la ejecución
	}
	session := getSessionMongo().Copy()
	defer session.Close()
	userC := session.DB(DBNAME).C(CNAME)
	err := userC.EnsureIndex(userIndex)
	if err != nil {
		log.Fatal(err)
	}
}

func InitMongo() {
	createDBSessionMongo()
	addIndex()
}
