package data

import (
	"time"

	"gitlab.com/arnaldoceballos/golang-mongodb/models/user"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	CNAME = "USERS"
)

type MongoUserStorage struct {
	context *ContextMongo
	c       *mgo.Collection
}

func (us *MongoUserStorage) setContext() {
	us.context = NewContextMongo()
	us.c = us.context.DBCollectionMongo(CNAME)
}

func (us MongoUserStorage) Create(u *user.User) error {
	obj_ID := bson.NewObjectId()
	u.ID = obj_ID
	hpass, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	u.HashPassword = hpass
	u.Password = ""
	us.setContext()               // Obtiene la sesión y la colección
	defer us.context.CloseMongo() // Que cierre el contexto (la sesión) al terminar la operación Create
	err = us.c.Insert(u)
	u.HashPassword = []byte{}
	return err
}

func (us MongoUserStorage) Update(u *user.User) error {
	us.setContext() // Obtiene la sesión y la colección
	u.UpdateAt = time.Now()
	defer us.context.CloseMongo() // Que cierre el contexto (la sesión) al terminar la operación Create

	err := us.c.Update(
		bson.M{"_id": u.ID},
		bson.M{"$set": bson.M{
			"username": u.UserName,
			"email":    u.Email,
			"updateat": time.Now(),
		}})
	return err
}

func (us MongoUserStorage) Delete(u *user.User) error {
	us.setContext()               // Obtiene la sesión y la colección
	defer us.context.CloseMongo() // Que cierre el contexto (la sesión) al terminar la operación Create
	err := us.c.RemoveId(u.ID)
	if err == mgo.ErrNotFound {
		return nil
	}
	return err
}

func (us MongoUserStorage) GetByID(u *user.User) error {
	us.setContext()               // Obtiene la sesión y la colección
	defer us.context.CloseMongo() // Que cierre el contexto (la sesión) al terminar la operación Create
	err := us.c.FindId(u.ID).One(u)
	u.HashPassword = []byte{}
	return err
}

func (us MongoUserStorage) GetAll() (user.Users, error) {
	var users user.Users
	us.setContext()                                // Obtiene la sesión y la colección
	defer us.context.CloseMongo()                  // Que cierre el contexto (la sesión) al terminar la operación Create
	iter := us.c.Find(nil).Sort("username").Iter() //-username: ordena de manera descendente
	u := user.New()
	for iter.Next(u) {
		u.HashPassword = []byte{}
		users = append(users, *u)
	}
	return users, nil

}
