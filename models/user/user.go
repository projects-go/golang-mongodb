package user

import (
	"time"
	"gopkg.in/mgo.v2/bson"
)

type Storage interface {
	Create(user *User) error
	Update(user *User) error
	Delete(user *User) error
	GetByID(user *User) error
	GetAll() (Users, error)
	
}

var storage Storage

// Esto permite vincular quien será el encargado de realizar las operaciones del storage. 
// En este ejemplo MongoUserStorage es quien implementa la interface Storage
func SetStorage(s Storage) {
	storage = s
}

type User struct {
	ID bson.ObjectId `bson:"_id,omitempty"`
	UserName string
	Email string
	Password string
	HashPassword []byte
	CreateAt time.Time
	UpdateAt time.Time
}

type Users []User

func New() *User {
	return &User{}
}

func (u *User) Create() error {
	// Se ejecuta el Create de quien implementa la interface Storage, en este caso MongoUserStorage
	return storage.Create(u)
}

func (u *User) Update() error {
	return storage.Update(u)
}

func (u *User) Delete() error {
	return storage.Delete(u)
}

func (u *User) GetByID() error {
	return storage.GetByID(u)
}

func (u *User) GetAll() (Users, error) {
	return storage.GetAll()
}
